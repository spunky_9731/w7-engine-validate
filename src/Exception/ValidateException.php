<?php

namespace W7\Validate\Exception;

class ValidateException extends \Exception
{
    protected string $attribute;

    public function __construct($message = '', $code = 0, string $attribute = '', \Throwable $previous = null)
    {
        $this->attribute = $attribute;
        parent::__construct($message, $code, $previous);
    }

    public function getAttribute(): string
    {
        return $this->attribute;
    }
}
