<?php

namespace W7\Validate\Support\Processor;

enum ProcessorOptions implements ProcessorSupport
{
    /**
     * When all parameters in the options except for the PreprocessorSupport class are values to be passed to the validator,
     * you need to use VALUE_IS_ARRAY to declare the value as an array; otherwise,
     * only the first parameter will be obtained as the default value.
     */
    case VALUE_IS_ARRAY;

    /**
     * When defining multiple data processors, you need to use this option,
     * which can only be placed at the top level of the array and is mutually exclusive with all other options.
     */
    case MULTIPLE;
}
