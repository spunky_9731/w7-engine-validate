<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Validate;

class TestValidateOrRule extends BaseTestValidate
{
    protected function checkHandler(Validate $v)
    {
        $data = $v->check([
            'username' => '995645888@qq.com'
        ]);
        $this->assertSame('995645888@qq.com', $data['username']);

        $data = $v->check([
            'username' => '18888889999'
        ]);
        $this->assertSame('18888889999', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^Username 必须满足 .*? 中的任意一个条件。$/');
        $v->check([
            'username' => '123456'
        ]);
    }

    /**
     * @test 测试在rule中直接定义or规则
     */
    public function testOrRule()
    {
        $v = new class extends Validate {
            protected $rule = [
                'username' => 'required|or:email,mobile'
            ];
        };

        $this->checkHandler($v);
    }

    /**
     * @test 测试or规则使用规则组
     */
    public function testOrRuleUseRuleGroup()
    {
        $v = new class extends Validate {
            protected $group = [
               'username' => 'email|mobile'
            ];

            protected $rule = [
                'username' => 'required|or:username'
            ];
        };

        $this->checkHandler($v);
    }

    /**
     * @test 测试or规则使用规则组和规则结合
     */
    public function testOrRuleUseRuleAndRuleGroup()
    {
        $v = new class extends Validate {
            protected $group = [
                'username' => 'email|mobile'
            ];

            protected $rule = [
                'username' => 'required|or:username,chs'
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);
        $this->checkHandler($v);
    }

    /**
     * @test 测试or规则使用多个规则组
     */
    public function testOrRuleUseMultipleRulep()
    {
        $v = new class extends Validate {
            protected $group = [
                'username'      => 'email|mobile',
                'usernameOther' => [
                    'chs', 'alpha'
                ]
            ];

            protected $rule = [
                'username' => 'required|or:username,usernameOther'
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);

        $data = $v->check([
            'username' => 'Yepyuyu'
        ]);
        $this->assertSame('Yepyuyu', $data['username']);
        $this->checkHandler($v);
    }
}
