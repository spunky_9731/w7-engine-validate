<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Support\Concerns\ProcessorInterface;
use W7\Validate\Support\DataAttribute;
use W7\Validate\Support\Processor\ProcessorExecCond;
use W7\Validate\Support\Processor\ProcessorOptions;
use W7\Validate\Support\Processor\ProcessorParams;
use W7\Validate\Support\ValidateScene;
use W7\Validate\Validate;

class UniqueFilter implements ProcessorInterface
{
    public function handle($value, string $attribute, array $originalData)
    {
        return array_unique($value);
    }
}
class TestDataPostProcessor extends BaseTestValidate
{
    public function testSetFilterIsSystemMethod()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required|numeric'
            ];

            protected $postprocessor = [
                'id' => ['intval', ProcessorParams::Value]
            ];
        };

        $data = $v->check(['id' => '1']);

        $this->assertTrue(1 === $data['id']);
    }

    public function testSetFilterIsClassMethod()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required'
            ];

            protected $postprocessor = [
                'id' => 'toArray'
            ];

            public function toArrayProcessor($value)
            {
                return explode(',', $value);
            }
        };

        $data = $v->check(['id' => '1,2,3,4,5']);

        $this->assertEquals([1, 2, 3, 4, 5], $data['id']);
    }

    public function testSetFilterIsFilterClass()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id'   => 'required|array',
                'id.*' => 'numeric'
            ];

            protected $postprocessor = [
                'id' => UniqueFilter::class
            ];
        };

        $data = $v->check(['id' => [1, 1, 2, 3, 4, 4, 5, 6, 7]]);

        $this->assertEquals([1, 2, 3, 4, 5, 6, 7], array_values($data['id']));
    }

    public function testSetFilterForArrayField()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id'   => 'required|array',
                'id.*' => 'numeric'
            ];

            protected $postprocessor = [
                'id.*' => ['intval', ProcessorParams::Value]
            ];
        };

        $data = $v->check(['id' => ['1', '2', 3, '4']]);

        foreach ($data['id'] as $id) {
            $this->assertSame('integer', gettype($id));
        }
    }

    /**
     * @test 测试当数据不存在时，过滤器的处理
     *
     * @throws ValidateException
     */
    public function testNotHasDataFilter()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'numeric'
            ];

            protected $postprocessor = [
                'id' => ['intval', ProcessorParams::Value]
            ];
        };

        $data = $v->check([]);
        $this->assertSame(0, $data['id']);
    }

    /**
     * @test 测试场景中取消设置过滤器
     *
     * @throws ValidateException
     */
    public function testCancelFilter()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required'
            ];

            protected $postprocessor = [
                'id' => ['intval', ProcessorParams::Value]
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['id'])
                    ->postprocessor('id', null);
            }
        };

        $data = $v->check(['id' => '你好']);
        $this->assertSame(0, $data['id']);
        $data = $v->scene('test')->check(['id' => '你好']);
        $this->assertSame('你好', $data['id']);
    }

    /**
     * @test 测试过滤器中删除字段
     *
     * @throws ValidateException
     */
    public function testFilterDeleteField()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => ''
            ];

            protected $postprocessor = [
                'a' => ['deleteField', ProcessorParams::DataAttribute]
            ];

            public function deleteFieldProcessor(DataAttribute $dataAttribute): string
            {
                $dataAttribute->deleteField = true;
                return '';
            }
        };

        $data = $v->check([
            'a' => 123
        ]);

        $this->assertTrue(empty($data));
    }

    /**
     * @test 测试使用后置处理器为字段设置默认值
     *
     * @throws ValidateException
     */
    public function testSetDefaultForPostProcessor()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'nullable'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['a'])->postprocessor('a', 'test', ProcessorExecCond::WHEN_EMPTY);
            }
        };

        $data = $v->scene('test')->check([]);
        $this->assertSame('test', $data['a']);

        $data = $v->scene('test')->check([
            'a' => 123
        ]);
        $this->assertSame(123, $data['a']);
    }

    /**
     * @test 测试使用过滤器删除数组元素
     *
     * @throws ValidateException
     */
    public function testDeleteArrayMember()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a'   => 'required|array',
                'a.*' => 'numeric'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['a', 'a.*'])
                    ->postprocessor('a.*', function ($value, string $attribute, array $originalData, DataAttribute $dataAttribute) {
                        if (0 == $value) {
                            $dataAttribute->deleteField = true;
                        }
                        return $value;
                    });
            }
        };

        $data = $v->scene('test')->check([
            'a' => [0, 1, 2, 3, 0, 4, 5, 6, 0]
        ]);

        $this->assertEquals([1, 2, 3, 4, 5, 6], array_values($data['a']));
    }

    public function testMultiplePostprocessor()
    {
        $v = new class extends Validate {
            protected $rule = [
                'test' => 'nullable|string'
            ];

            protected $postprocessor = [
                'test' => [
                    ['trim', ProcessorParams::Value],
                    ['base64_encode', ProcessorParams::Value],
                    ProcessorOptions::MULTIPLE
                ],
            ];
        };

        $data = $v->check([
            'test' => '  test  '
        ]);

        $this->assertSame('dGVzdA==', $data['test']);
    }

    public function testMultiplePostprocessorInScene()
    {
        $v = new class extends Validate {
            protected $rule = [
                'test' => 'nullable|string'
            ];

            protected function sceneMultiple(ValidateScene $scene)
            {
                $scene->only(['test'])
                    ->postprocessor('test', 'trim', ProcessorParams::Value)
                    ->postprocessor('test', 'base64_encode', ProcessorParams::Value);
            }
        };

        $data = $v->check([
            'test' => '  test  '
        ]);

        $this->assertSame('  test  ', $data['test']);

        $data = $v->scene('multiple')->check([
            'test' => '  test  '
        ]);

        $this->assertSame('dGVzdA==', $data['test']);
    }
}
